/*
 *
 *  BlueZ - Bluetooth protocol stack for Linux
 *
 *  Copyright (C) 2011-2014  Intel Corporation
 *  Copyright (C) 2004-2010  Marcel Holtmann <marcel@holtmann.org>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <time.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "lib/bluetooth.h"
#include "lib/hci.h"

#include "src/shared/mainloop.h"
#include "btdev.h"
#include "vhci.h"
#include "server.h"

#define uninitialized_var(x) x = x

struct server {
	enum server_type type;
	uint16_t id;
	int fd;
	struct vhci *vhci;
};

struct vhci {
	enum vhci_type type;
	int fd;
	struct btdev *btdev;
};

struct client {
	int fd;
	struct btdev *btdev;
	uint8_t *pkt_data;
	uint8_t pkt_type;
	uint16_t pkt_expect;
	uint16_t pkt_len;
	uint16_t pkt_offset;
};



//struct client *ccc;

static void server_destroy(void *user_data)
{
	struct server *server = user_data;

	close(server->fd);

	free(server);
}

static void client_destroy(void *user_data)
{
	struct client *client = user_data;

	//btdev_destroy(client->btdev);
	//printf("client_destroy \n");

	close(client->fd);

	free(client);
}

static void client_write_callback(const struct iovec *iov, int iovlen,
							void *user_data)
{
	struct client *client = user_data;
	struct msghdr msg;
	ssize_t written;

	memset(&msg, 0, sizeof(msg));

	msg.msg_iov = (struct iovec *) iov;
	msg.msg_iovlen = iovlen;

	written = sendmsg(client->fd, &msg, MSG_DONTWAIT);
	if (written < 0)
		return;
}


static void client_read_callback(int fd, uint32_t events, void *user_data)
{
	struct client *client = user_data;
	static uint8_t buf[4096];
	uint8_t *ptr = buf;
	ssize_t len;
	uint16_t count;

    //    printf("client_read_callback start\n");
	//len = recv(fd, buf + client->pkt_offset,
	//		sizeof(buf) - client->pkt_offset, MSG_DONTWAIT);
	len = recv(fd, buf,sizeof(buf), MSG_DONTWAIT);
	//printf("buf: %s\n",buf);
	//printf("buf[0]: %d\n",buf[0]);
	//printf("bdaddr: %x %x %x %x %x %x\n", buf[6],buf[5],buf[4],buf[3],buf[2],buf[1]);

	int rssi = 0 - buf[0];

	uint16_t id = rand() % 100000;
        uint8_t addr[6] = {buf[6],buf[5],buf[4],buf[3],buf[2],buf[1]};
	uint8_t *addr_p = addr;

        uint8_t adv_type = buf[7];


	struct btdev *remote_btdev;
        remote_btdev = btdev_create_with_bdaddr(BTDEV_TYPE_BREDRLE, id, addr_p);

	//printf("bdaddr: %x %x %x %x %x %x \n", addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);

	if (events & (EPOLLERR | EPOLLHUP)) {
		mainloop_remove_fd(client->fd);
		return;
	}
	//le_send_adv_report(client->btdev, remote_btdev, adv_type, rssi);
	btdev_destroy(remote_btdev);
    //    printf("client fd remove: %d\n",client->fd);
	mainloop_remove_fd(client->fd);
/*
again:
	if (len < 0) {
		if (errno == EAGAIN)
			goto again;
		return;
	}
	if (!client->btdev)
		return;
	count = client->pkt_offset + len;
	while (count > 0) {
		hci_command_hdr *cmd_hdr;
		if (!client->pkt_data) {
			client->pkt_type = ptr[0];
			switch (client->pkt_type) {
			case HCI_COMMAND_PKT:
				if (count < HCI_COMMAND_HDR_SIZE + 1) {
					client->pkt_offset += len;
					return;
				}
				cmd_hdr = (hci_command_hdr *) (ptr + 1);
				client->pkt_expect = HCI_COMMAND_HDR_SIZE +
							cmd_hdr->plen + 1;
				client->pkt_data = malloc(client->pkt_expect);
				client->pkt_len = 0;
				break;
			default:
				printf("packet error\n");
				return;
			}
			client->pkt_offset = 0;
		}
		if (count >= client->pkt_expect) {
			memcpy(client->pkt_data + client->pkt_len,
						ptr, client->pkt_expect);
			ptr += client->pkt_expect;
			count -= client->pkt_expect;
			btdev_receive_h4(client->btdev, client->pkt_data,
					client->pkt_len + client->pkt_expect);
			free(client->pkt_data);
			client->pkt_data = NULL;
		} else {
			memcpy(client->pkt_data + client->pkt_len, ptr, count);
			client->pkt_len += count;
			client->pkt_expect -= count;
			count = 0;
		}
	}
	btdev_destroy(remote_btdev);
	mainloop_remove_fd(client->fd);
*/
}



/*
static void client_read_callback(int fd, uint32_t events, void *user_data)
{
	//struct client *client = user_data;
	struct client *ccc = user_data;
	const struct btdev *dummydev;
	static uint8_t buf[4096];
	uint8_t *ptr = buf;
	ssize_t len;
	uint16_t count;
	dummydev = ccc->btdev;
	le_send_adv_report(ccc->btdev, dummydev, 0x04);

	if (events & (EPOLLERR | EPOLLHUP)) {
		mainloop_remove_fd(ccc->fd);
		return;
	}

again:
	//len = recv(fd, buf + ccc->pkt_offset,
	//		sizeof(buf) - ccc->pkt_offset, MSG_DONTWAIT);
	len = recv(fd, buf, sizeof(buf), MSG_DONTWAIT);
	if (len > 0) {
		printf("client read callback len: %d\n",len);
		printf("%s\n",buf);
		dummydev = ccc->btdev;
		le_send_adv_report(ccc->btdev, dummydev, 0x04);

	}
	if (len < 0) {
		if (errno == EAGAIN)
			goto again;
		return;
	}

	if (!ccc->btdev)
		return;

	count = ccc->pkt_offset + len;

	while (count > 0) {
		hci_command_hdr *cmd_hdr;
		hci_acl_hdr *acl_hdr;

		if (!ccc->pkt_data) {
			ccc->pkt_type = ptr[0];
			printf("ptr0 %x\n",ptr[0]);
			printf("ptr1 %x\n",ptr[1]);
			printf("ptr2 %x\n",ptr[2]);
			printf("HCI_COMMAND_PKT: %x\n",HCI_COMMAND_PKT);
			printf("HCI_ACLDATA_PKT: %x\n",HCI_ACLDATA_PKT);

			switch (ccc->pkt_type) {
			case HCI_COMMAND_PKT:
				printf("HCI COMMAND PKT\n");
				le_send_adv_report(ccc->btdev, ccc->btdev, 0x04);
				if (count < HCI_COMMAND_HDR_SIZE + 1) {
					printf("here?\n");
					ccc->pkt_offset += len;
					return;
				}
				cmd_hdr = (hci_command_hdr *) (ptr + 1);
				ccc->pkt_expect = HCI_COMMAND_HDR_SIZE +
							cmd_hdr->plen + 1;
				ccc->pkt_data = malloc(ccc->pkt_expect);
				ccc->pkt_len = 0;
				break;

			case HCI_ACLDATA_PKT:
				acl_hdr = (hci_acl_hdr*)(ptr + 1);
				client->pkt_expect = HCI_ACL_HDR_SIZE + acl_hdr->dlen + 1;
				client->pkt_data = malloc(client->pkt_expect);
				client->pkt_len = 0;
				break;

			default:
				printf("packet error\n\n");
				return;
			}

			ccc->pkt_offset = 0;
		}/accept
		printf("count1 %d\n",count);
		//printf("expect1 %d\n",client->pkt_expect);


		if (count >= ccc->pkt_expect) {
			memcpy(ccc->pkt_data + ccc->pkt_len,
						ptr, ccc->pkt_expect);
			ptr += ccc->pkt_expect;
			count -= ccc->pkt_expect;

			btdev_receive_h4(ccc->btdev, ccc->pkt_data,
					ccc->pkt_len + ccc->pkt_expect);

			free(ccc->pkt_data);
			ccc->pkt_data = NULL;
		} else {
			memcpy(ccc->pkt_data + ccc->pkt_len, ptr, count);
			ccc->pkt_len += count;
			ccc->pkt_expect -= count;
			count = 0;
		}
		printf("count2 %d\n",count);
		//printf("expect2 %d\n",client->pkt_expect);
		printf("----------------------\n");

	}
}
*/

static int accept_client(int fd)
{
	struct sockaddr_un addr;
	socklen_t len;
	int nfd;

	memset(&addr, 0, sizeof(addr));

	len = sizeof(addr);

	if (getsockname(fd, (struct sockaddr *) &addr, &len) < 0) {
		perror("Failed to get socket name");
		return -1;
	}

	//printf("Request for %s\n", addr.sun_path);

	nfd = accept(fd, (struct sockaddr *) &addr, &len);
	//printf ("fd: %d\n",fd);
	printf ("nfd: %d\n",nfd);
        nfd = fd;
	if (nfd < 0) {
		perror("Failed to accept client socket");
		return -1;
	}

	return nfd;
	//return 0;
}


static void udpserver_accept_callback(int fd, uint32_t events, void *user_data)
{
	printf("UDP Server accept callback\n");
	struct server *server = user_data;
	struct client *client;
	struct vhci *vhci;
	struct btdev *btdev;

	vhci = server->vhci;

	client = malloc(sizeof(*client));
	memset(client, 0, sizeof(*client));
	client->fd = server->fd;

	client->btdev = vhci->btdev;
	btdev = client->btdev;
	btdev_set_send_handler(client->btdev, vhci_write_callback, vhci);

	enum btdev_type uninitialized_var(type);

	if (events & (EPOLLERR | EPOLLHUP)) {
		mainloop_remove_fd(server->fd);
		return;
	}

	static uint8_t buf[4096];
	uint8_t *ptr = buf;
	ssize_t len;
	uint16_t count;


	//len = recv(fd, buf + client->pkt_offset,
	//		sizeof(buf) - client->pkt_offset, MSG_DONTWAIT);
	len = recv(fd, buf,sizeof(buf), MSG_DONTWAIT);

    uint8_t ch = buf[0];
    int8_t rssi = buf[1];
    uint8_t preamble = buf[2];
    uint16_t id = rand() % 100000;
    uint8_t access_addr[4] = {buf[3],buf[4],buf[5],buf[6]};
    uint8_t adv_addr[4] = {0x8e,0x89,0xbe,0xd6};

    uint8_t adv_type = buf[7] >> 4;
    uint8_t txaddr = buf[7] >> 1;
    uint8_t pdu_len = buf[8] >> 2;
    uint8_t addr[6] = {buf[9],buf[10],buf[11],buf[12],buf[13],buf[14]};
    uint8_t *addr_p = addr;

    int datalen = buf[15];
    uint8_t data[128];
    uint8_t *dataptr = data;

    uint8_t lse;
    struct btdev* remote_btdev;

    printf("0-3: %x %x %x %x \n",buf[0],buf[1],buf[2],buf[3]);
    printf("4-7: %x %x %x %x \n",buf[4],buf[5],buf[6],buf[7]);


    if (!memcmp(access_addr,adv_addr,4)){

        remote_btdev = find_btdev_by_bdaddr(addr_p);
        if(!remote_btdev) {
            remote_btdev = btdev_create_with_bdaddr(BTDEV_TYPE_BREDRLE, id, addr_p);
        }

        if (btdev->le_init_enable == 1){
            if(!memcmp(btdev->le_init_direct_addr,addr,6)){
                btdev->le_init_enable = 0;
                btdev->le_scan_enable = 0;
                le_conn_request(btdev);
            }

        } else if (btdev->le_scan_enable) {
       // } else {

            //remote_btdev = btdev_create_with_bdaddr(BTDEV_TYPE_BREDRLE, id, addr_p);

    	//printf("bdaddr: %x %x %x %x %x %x \n", addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);


            for (int i=0;i<pdu_len;i++){
                data[i] = buf[i+13];
            }
            le_send_adv_report(client->btdev, remote_btdev, adv_type, rssi, datalen, dataptr);
            //btdev_destroy(remote_btdev);
        }
    } else {
        for (int i=0;i<pdu_len;i++){
            data[i] = buf[i+13];
        }
        send_acl(btdev,data,pdu_len);
    }

/*
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv,&tz);

        uint64_t ms = (tv.tv_sec % 60) * 1000000 + tv.tv_usec;
        uint8_t timestamp[4];
        timestamp[0] = (ms>>24) & 0xff;
        timestamp[1] = (ms>>16) & 0xff;
        timestamp[2] = (ms>>8) & 0xff;
        timestamp[3] = ms & 0xff;
        //printf("----tv_sec/60 %lu\n",ts.tv_sec%60);
        printf("----ms %lu\n",ms);
        printf("----timestamp");
        uint8_t timestampheader[4] = {0x07,0x16,0x02,0x00};
        for (int i=0;i<4;i++){
                data[datalen+i] = timestampheader[i];
                data[datalen+4+i] = timestamp[i];
                printf(" %x",timestamp[i]);
	}
        datalen = datalen + 8;
*/
//	memcpy(buf[9],data,datalen*sizeof(uint8_t));


//	uint8_t ddd[12] = {0x12,0x09,0x4D,0x45,0x53,0x48,0x2D,0x31,0x30,0x2D,0x31,0x30};
//	uint8_t *dataptr = ddd;
	//uint8_t ddd[10] = {0x02,0x01,0x02,0x02,0x0a,0x02,0x03,0x02,0x0f,0x18};
	//uint8_t *dataptr2 = ddd;

	if (events & (EPOLLERR | EPOLLHUP)) {
		mainloop_remove_fd(server->fd);
		return;
	}



	//le_send_adv_report(client->btdev, remote_btdev, adv_type, rssi, 10, dataptr2);

//	mainloop_remove_fd(client->fd);
}





static void server_accept_callback(int fd, uint32_t events, void *user_data)
{
	printf("Server accept callback\n");
	struct server *server = user_data;
	struct client *client;
	struct vhci *vhci;
	vhci = server->vhci;
	enum btdev_type uninitialized_var(type);

	if (events & (EPOLLERR | EPOLLHUP)) {
		mainloop_remove_fd(server->fd);
		return;
	}

	client = malloc(sizeof(*client));

	if (!client)
		return;

	memset(client, 0, sizeof(*client));


	printf("Before accept client\n");
	client->fd = accept_client(server->fd);
	printf("After accept client:%d\n",client->fd);
	if (client->fd < 0) {
		free(client);
		return;
	}

	switch (server->type) {
	case SERVER_TYPE_BREDRLE:
		type = BTDEV_TYPE_BREDRLE;
		break;
	case SERVER_TYPE_BREDR:
		type = BTDEV_TYPE_BREDR;
		break;
	case SERVER_TYPE_LE:
		type = BTDEV_TYPE_LE;
		break;
	case SERVER_TYPE_AMP:
		type = BTDEV_TYPE_AMP;
		break;
	case SERVER_TYPE_ADVINSERT:
 		type = SERVER_TYPE_ADVINSERT;
		break;
	case SERVER_TYPE_MONITOR:
		goto done;
	}
    if (type == SERVER_TYPE_ADVINSERT) {
        client->btdev = vhci->btdev;
    	if (!client->btdev) {
    		close(client->fd);
    		free(client);
    		return;
    	}
        //le_send_adv_report(client->btdev, client->btdev, 0x04);
    }


	btdev_set_send_handler(client->btdev, vhci_write_callback, vhci);

done:
	if (mainloop_add_fd(client->fd, EPOLLIN, client_read_callback,
						client, client_destroy) < 0) {
		btdev_destroy(client->btdev);
		close(client->fd);
		free(client);
		printf("done\n");
	}

}

static int open_unix(const char *path)
{
	struct sockaddr_un addr;
	int fd;

	unlink(path);

	fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("Failed to open server socket");
		return -1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, path);

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("Failed to bind server socket");
		close(fd);
		return -1;
	}

	if (listen(fd, 5) < 0) {
		perror("Failed to listen server socket");
		close(fd);
		return -1;
	}

	return fd;
}

struct server *server_open_unix(enum server_type type, const char *path)
{
	struct server *server;

	server = malloc(sizeof(*server));
	if (!server)
		return NULL;

	memset(server, 0, sizeof(*server));
	server->type = type;
	server->id = 0x42;

	server->fd = open_unix(path);
	if (server->fd < 0) {
		free(server);
		return NULL;
	}

	if (mainloop_add_fd(server->fd, EPOLLIN, server_accept_callback,
						server, server_destroy) < 0) {
		close(server->fd);
		free(server);
		return NULL;
	}

	return server;
}

static int open_tcp(void)
{
	struct sockaddr_in addr;
	int fd, opt = 1;
        // TCP
	fd = socket(PF_INET, SOCK_STREAM, 0);


	if (fd < 0) {
		perror("Failed to open server socket");
		return -1;
	}

	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(45550);

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("Failed to bind server socket");
		close(fd);
		return -1;
	}


	if (listen(fd, 5) < 0) {
		perror("Failed to listen server socket");
		close(fd);
		return -1;
	}

	return fd;
}


static int open_udp(void)
{
	struct sockaddr_in addr;
	int fd, opt = 1;

	// UDP
	fd = socket(PF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		perror("Failed to open server socket");
		return -1;
	}

	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	//addr.sin_addr.s_addr = inet_addr("192.168.0.211");
	addr.sin_port = htons(30000);

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("Failed to bind server socket");
		close(fd);
		return -1;
	}

/*
	if (listen(fd, 5) < 0) {
		perror("Failed to listen server socket");
		close(fd);
		return -1;
	}
*/
	return fd;
}

struct server *server_open_tcp(enum server_type type)
{
	struct server *server;

	server = malloc(sizeof(*server));
	if (!server)
		return server;

	memset(server, 0, sizeof(*server));
	server->type = type;
	server->id = 0x43;

	server->fd = open_tcp();
	if (server->fd < 0) {
		free(server);
		return NULL;
	}

	if (mainloop_add_fd(server->fd, EPOLLIN, server_accept_callback,
						server, server_destroy) < 0) {
		close(server->fd);
		free(server);
		return NULL;
	}

	return server;
}



void server_close(struct server *server)
{
	if (!server)
		return;

	mainloop_remove_fd(server->fd);
}


struct server *server_open_tcp_with_vhci(struct vhci *vhci)
{
	struct server *server;

	server = malloc(sizeof(*server));
	if (!server)
		return server;

	memset(server, 0, sizeof(*server));
	server->type = SERVER_TYPE_ADVINSERT;
	server->id = 0x43;
   	server->vhci = vhci;

	server->fd = open_tcp();
	if (server->fd < 0) {
		free(server);
		return NULL;
	}

	if (mainloop_add_fd(server->fd, EPOLLIN, server_accept_callback,
						server, server_destroy) < 0) {
		close(server->fd);
		free(server);
		return NULL;
	}

	return server;
}

struct server *server_open_udp_with_vhci(struct vhci *vhci)
{
	struct server *server;

	server = malloc(sizeof(*server));
	if (!server)
		return server;

	memset(server, 0, sizeof(*server));
	server->type = SERVER_TYPE_ADVINSERT;
	server->id = 0x43;
   	server->vhci = vhci;

	server->fd = open_udp();
	if (server->fd < 0) {
		free(server);
		return NULL;
	}

	if (mainloop_add_fd(server->fd, EPOLLIN, udpserver_accept_callback,
						server, server_destroy) < 0) {
		close(server->fd);
		free(server);
		return NULL;
	}

	return server;
}
