/*
 *
 *  BlueZ - Bluetooth protocol stack for Linux
 *
 *  Copyright (C) 2011-2014  Intel Corporation
 *  Copyright (C) 2004-2010  Marcel Holtmann <marcel@holtmann.org>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "lib/bluetooth.h"
#include "lib/hci.h"

#include "src/shared/mainloop.h"
#include "monitor/bt.h"
#include "btdev.h"
#include "vhci.h"
#include "server.h"

#define uninitialized_var(x) x = x

struct vhci {
	enum vhci_type type;
	int fd;
	struct btdev *btdev;
};

struct server {
	enum server_type type;
	uint16_t id;
	int fd;
	struct vhci *vhci;
};

static void vhci_destroy(void *user_data)
{
	struct vhci *vhci = user_data;
	printf("called vhci_destroy");

	btdev_destroy(vhci->btdev);

	close(vhci->fd);

	free(vhci);
}

static void adv_destroy(void *user_data)
{
	//struct vhci *vhci = user_data;

	//btdev_destroy(vhci->btdev);

	printf("called adv_destroy");
	close(8);

	//free(vhci);
}

void vhci_write_callback(const struct iovec *iov, int iovlen,
							void *user_data)
{
	struct vhci *vhci = user_data;
	ssize_t written;

	printf("vhci write callback\n");
	written = writev(vhci->fd, iov, iovlen);
	if (written < 0)
		return;
}

static void vhci_read_callback(int fd, uint32_t events, void *user_data)
{
	struct vhci *vhci = user_data;
	unsigned char buf[4096];
	ssize_t len;


	if (events & (EPOLLERR | EPOLLHUP))
		return;

	//len = read(vhci->fd, buf, sizeof(buf));
	len = read(fd, buf, sizeof(buf));
        
	printf("vhci read callback %x \n",buf[0]);
        printf("vhci read len %d \n",len);
	for (int i=0; i<len; i++){
	    printf("%x",buf[i]);
	}
	printf("\n");

	//le_send_adv_report(vhci->btdev,vhci->btdev, 0x04);
	if (len < 1)
		return;

	switch (buf[0]) {
	case BT_H4_CMD_PKT:
	case BT_H4_ACL_PKT:
	case BT_H4_SCO_PKT:
		btdev_receive_h4(vhci->btdev, buf, len);
		break;
	}
}

static void adv_callback(int fd, uint32_t events, void *user_data)
{
	//struct vhci *vhci = user_data;
	unsigned char buf[4096];
	ssize_t len;

	if (events & (EPOLLERR | EPOLLHUP))
		return;

	//len = read(fd, buf, sizeof(buf));
	len = recv(fd, buf, sizeof(buf), 0);
	perror("recv");
	sleep(1);

        //close(fd);
	//if (len < 1)
	//	return;
        //printf("adv len %d \n",len);
        //printf("adv buf %s \n",&buf);
	//le_send_adv_report(vhci->btdev,vhci->btdev, 0x04);
	if (len > -1)
        	printf("adv callback %d \n",len);
}

struct vhci *vhci_open(enum vhci_type type)
{
	struct vhci *vhci;
	enum btdev_type uninitialized_var(btdev_type);
	unsigned char uninitialized_var(ctrl_type);
	unsigned char setup_cmd[2];
	static uint8_t id = 0x23;

	//struct server *adv_server;

	switch (type) {
	case VHCI_TYPE_BREDRLE:
		btdev_type = BTDEV_TYPE_BREDRLE;
		ctrl_type = HCI_PRIMARY;
		break;
	case VHCI_TYPE_BREDR:
		btdev_type = BTDEV_TYPE_BREDR;
		ctrl_type = HCI_PRIMARY;
		break;
	case VHCI_TYPE_LE:
		btdev_type = BTDEV_TYPE_LE;
		ctrl_type = HCI_PRIMARY;
		break;
	case VHCI_TYPE_AMP:
		btdev_type = BTDEV_TYPE_AMP;
		ctrl_type = HCI_AMP;
		break;
	}

	vhci = malloc(sizeof(*vhci));
	if (!vhci)
		return NULL;

	memset(vhci, 0, sizeof(*vhci));
	vhci->type = type;

	vhci->fd = open("/dev/vhci", O_RDWR | O_NONBLOCK);
	if (vhci->fd < 0) {
		free(vhci);
		return NULL;
	}

	setup_cmd[0] = HCI_VENDOR_PKT;
	setup_cmd[1] = ctrl_type;

	if (write(vhci->fd, setup_cmd, sizeof(setup_cmd)) < 0) {
		close(vhci->fd);
		free(vhci);
		return NULL;
	}

	vhci->btdev = btdev_create(btdev_type, id++);
	if (!vhci->btdev) {
		close(vhci->fd);
		free(vhci);
		return NULL;
	}

	btdev_set_send_handler(vhci->btdev, vhci_write_callback, vhci);

	if (mainloop_add_fd(vhci->fd, EPOLLIN, vhci_read_callback,
						vhci, vhci_destroy) < 0) {
		btdev_destroy(vhci->btdev);
		close(vhci->fd);
		free(vhci);
		return NULL;
	}

	/* add 20170124 */
	//fd_adv = open_unix_adv("/tmp/adv.sock");
	//server_open_tcp_with_vhci(vhci);
	server_open_udp_with_vhci(vhci);
	printf("1: %x\n",vhci->btdev);
/*
	//fd_adv = open("/tmp/adv.sock", O_RDWR | O_NONBLOCK);
	if (fd_adv < 0) {
		printf("fail fd_adv open\n");
		//return NULL;
	}
	if (mainloop_add_fd(fd_adv, EPOLLIN, adv_callback,
						NULL, adv_destroy) < 0) {
		printf("0000000000000000!!!!!!!!!");
		//btdev_destroy(vhci->btdev);
		close(fd_adv);
		//free(vhci);
		return NULL;
	}
*/
	return vhci;
}

void vhci_close(struct vhci *vhci)
{
	if (!vhci)
		return;

	mainloop_remove_fd(vhci->fd);
}


/*
int adv_open_tcp(void)
{
	struct sockaddr_in addr;
	int fd, opt = 1;

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("Failed to open server socket");
		return -1;
	}

	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(45550);

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("Failed to bind server socket");
		close(fd);
		return -1;
	}

	if (listen(fd, 5) < 0) {
		perror("Failed to listen server socket");
		close(fd);
		return -1;
	}

	return fd;
}
*/



int open_unix_adv(const char *path)
{
	struct sockaddr_un addr;
	int fd;

        printf("PATH:%s",path);

	unlink(path);

	fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("Failed to open server socket");
		return -1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, path);

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("Failed to bind server socket");
		close(fd);
		return -1;
	}

	if (listen(fd, 5) < 0) {
		perror("Failed to listen server socket");
		close(fd);
		return -1;
	}

	return fd;
}


