/*
 *
 *  BlueZ - Bluetooth protocol stack for Linux
 *
 *  Copyright (C) 2011-2012  Intel Corporation
 *  Copyright (C) 2004-2010  Marcel Holtmann <marcel@holtmann.org>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdint.h>
#include <stdbool.h>

#define BTDEV_RESPONSE_DEFAULT		0
#define BTDEV_RESPONSE_COMMAND_STATUS	1
#define BTDEV_RESPONSE_COMMAND_COMPLETE	2
#define BTDEV_ADVERTISE_PREAMBLE 0xaa

typedef struct btdev_callback * btdev_callback;

void btdev_command_response(btdev_callback callback, uint8_t response,
				uint8_t status, const void *data, uint8_t len);

#define btdev_command_default(callback) \
		btdev_command_response(callback, \
			BTDEV_RESPONSE_DEFAULT, 0x00, NULL, 0);

#define btdev_command_status(callback, status) \
		btdev_command_response(callback, \
			BTDEV_RESPONSE_COMMAND_STATUS, status, NULL, 0);

#define btdev_command_complete(callback, data, len) \
		 btdev_command_response(callback, \
			BTDEV_RESPONSE_COMMAND_COMPLETE, 0x00, data, len);


typedef void (*btdev_command_func) (uint16_t opcode,
				const void *data, uint8_t len,
				btdev_callback callback, void *user_data);

typedef void (*btdev_send_func) (const struct iovec *iov, int iovlen,
							void *user_data);

typedef bool (*btdev_hook_func) (const void *data, uint16_t len,
							void *user_data);

enum btdev_type {
	BTDEV_TYPE_BREDRLE,
	BTDEV_TYPE_BREDR,
	BTDEV_TYPE_LE,
	BTDEV_TYPE_AMP,
	BTDEV_TYPE_BREDR20,
};

enum btdev_hook_type {
	BTDEV_HOOK_PRE_CMD,
	BTDEV_HOOK_POST_CMD,
	BTDEV_HOOK_PRE_EVT,
	BTDEV_HOOK_POST_EVT,
};

//struct btdev;

#define MAX_HOOK_ENTRIES 16

struct btdev{

    enum btdev_type type;

    struct btdev *conn;

    bool auth_init;
    uint8_t link_key[16];
    uint16_t pin[16];
    uint8_t pin_len;
    uint8_t io_cap;
    uint8_t auth_req;
    bool ssp_auth_complete;
    uint8_t ssp_status;

    btdev_command_func command_handler;
    void *command_data;

    btdev_send_func send_handler;
    void *send_data;

    unsigned int inquiry_id;
    unsigned int inquiry_timeout_id;

    struct hook *hook_list[MAX_HOOK_ENTRIES];

    struct bt_crypto *crypto;

    uint16_t manufacturer;
    uint8_t  version;
    uint16_t revision;
    uint8_t  commands[64];
    uint8_t  max_page;
    uint8_t  features[8];
    uint8_t  feat_page_2[8];
    uint16_t acl_mtu;
    uint16_t acl_max_pkt;
    uint8_t  country_code;
    uint8_t  bdaddr[6];
    uint8_t  random_addr[6];
    uint8_t  le_features[8];
    uint8_t  le_states[8];

    uint16_t default_link_policy;
    uint8_t  event_mask[8];
    uint8_t  event_mask_page2[8];
    uint8_t  event_filter;
    uint8_t  name[248];
    uint8_t  dev_class[3];
    uint16_t voice_setting;
    uint16_t conn_accept_timeout;
    uint16_t page_timeout;
    uint8_t  scan_enable;
    uint16_t page_scan_interval;
    uint16_t page_scan_window;
    uint16_t page_scan_type;
    uint8_t  auth_enable;
    uint16_t inquiry_scan_interval;
    uint16_t inquiry_scan_window;
    uint8_t  inquiry_mode;
    uint8_t  afh_assessment_mode;
    uint8_t  ext_inquiry_fec;
    uint8_t  ext_inquiry_rsp[240];
    uint8_t  simple_pairing_mode;
    uint8_t  ssp_debug_mode;
    uint8_t  secure_conn_support;
    uint8_t  host_flow_control;
    uint8_t  le_supported;
    uint8_t  le_simultaneous;
    uint8_t  le_event_mask[8];
    uint8_t  le_adv_data[31];
    uint8_t  le_adv_data_len;
    uint8_t  le_adv_type;
    uint8_t  le_adv_own_addr;
    uint8_t  le_adv_direct_addr_type;
    uint8_t  le_adv_direct_addr[6];
    uint8_t  le_scan_data[31];
    uint8_t  le_scan_data_len;
    uint8_t  le_scan_enable;
    uint8_t  le_scan_type;
    uint8_t  le_scan_own_addr_type;
    uint8_t  le_filter_dup;
    uint8_t  le_adv_enable;
    uint8_t  le_init_enable;
    uint8_t  le_init_direct_addr_type;
    uint8_t  le_init_direct_addr[6];
    uint8_t  le_conn_access_addr[4];
    uint8_t  le_ltk[16];

    uint8_t le_local_sk256[32];

    uint16_t sync_train_interval;
    uint32_t sync_train_timeout;
    uint8_t  sync_train_service_data;
};


struct btdev *btdev_create(enum btdev_type type, uint16_t id);
struct btdev *btdev_create_with_bdaddr(enum btdev_type type, uint16_t id, uint8_t *addr);
struct btdev *find_btdev_by_bdaddr(const uint8_t *bdaddr);
void btdev_destroy(struct btdev *btdev);

const uint8_t *btdev_get_bdaddr(struct btdev *btdev);
uint8_t *btdev_get_features(struct btdev *btdev);

uint8_t btdev_get_scan_enable(struct btdev *btdev);

uint8_t btdev_get_le_scan_enable(struct btdev *btdev);

void btdev_set_command_handler(struct btdev *btdev, btdev_command_func handler,
							void *user_data);

void btdev_set_send_handler(struct btdev *btdev, btdev_send_func handler,
							void *user_data);

void btdev_receive_h4(struct btdev *btdev, const void *data, uint16_t len);

int btdev_add_hook(struct btdev *btdev, enum btdev_hook_type type,
				uint16_t opcode, btdev_hook_func handler,
				void *user_data);

bool btdev_del_hook(struct btdev *btdev, enum btdev_hook_type type,
							uint16_t opcode);

void le_send_adv_report(struct btdev *btdev, const struct btdev *remote,
			uint8_t type, int rssi, int datalen, uint8_t *data);

void le_conn_request(struct btdev *btdev);

void send_acl(struct btdev *conn, const void *data, uint16_t len);
