from fabric.api import run,env,local,shell_env,cd
env.use_ssh_config = True
env.hosts = ['n211']

def push():
    local("git push origin dev")

def make():
    with cd('/home/yumu/bluez'):
        run('make emulator/btvirt')

def deploy():
    push()
    with cd('/home/yumu/bluez'):
        run('git pull origin dev')
        # with shell_env(FLASK_APP='app.py'):
        #     run('flask run')

